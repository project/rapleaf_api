<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Language" content="en" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rapleaf Address Book API Sample - PHP</title>
<body style="padding-left:50px;padding-right:50px;background-color:#ddeedd;">
<h1>Rapleaf Address Book API Sample</h1>

<form action="sample.php" method="post">
	<table>
	<tr><td>Email: </td><td><input type="text" name="email" size="40"/></td></tr>
	<tr><td>Password: </td><td>	<input type="password" name="pass" size="40"/></td></tr>
	<tr><td><a href="http://www.rapleaf.com/developer/api_key">API Key:</a> </td><td><input type="text" name="api_key" size="40" /></td></tr>	
	</table>
	<input type="submit" value="Import Contacts" />
</form>
<hr />

<?php
require_once 'rapleaf_abook.php';

if (sizeof($_POST) > 1) {
	$abook = new RapleafAbook($_POST['api_key']);
	$result = $abook->getData(urlencode($_POST['email']), $_POST['pass']);
	$html = '';
	if ($result['status']=='200') { # OK
		$contacts = $result['contacts'];
		$html = '<h2>My Contacts ('.sizeof($contacts).')</h2>';
		foreach ($contacts as $contact) {
			$html.= '<input type="checkbox" checked>'.htmlentities($contact['name'].' <'.$contact['email'].'>');
			$html.='<br />';
		}
		$html.='<input type="submit" value="Continue" />';
	} else {
		$html = $result['status'].': '.$result['error'];
	}
	
	echo $html;
}
?>

</body>
</html>