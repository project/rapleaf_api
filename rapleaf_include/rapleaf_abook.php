<?php
/*
 * Helper class for the Rapleaf Address Book API
 * 		(http://www.rapleaf.com/apidoc/v2/abook)
 * Gets the XML response from the Rapleaf server and parses it into a PHP object.
 *
 * Usage: 
 * 	1. $abook = new RapleafAbook(api_key[,url]) to initialize
 *  2. $result = $abook->getData(email, password) to query the API for a contact list
 *  See the function definitions for details.
 *  
 * 03/01/2008
 *
 */
 
class RapleafAbook {
	var $url;
	var $api_key;
	var $status; 

	function RapleafAbook($api_key, $url = 'http://api.rapleaf.com/v2/abook') {
		$this->api_key = $api_key;
		$this->url = $url;
		$this->status = '';
	}

	function getData($email, $pass) {
		# assemble post_data string
		$post_data = "login=$email&password=$pass";
		$response = $this->sendPostRequest($this->url, $post_data);
    
		# the return structure
		$result = array(
			'status'   => '',  # HTTP status code returned by the server
			'error'    => '',  # error message if there are any
			'contacts' => array(),  # contact list if request succeeded
		);
		
		$result['status'] = $this->status;
		if ($this->status == '200') { #OK
			$result['contacts'] = $this->xmlToObj($response);
		} elseif ($this->status == '400') {
			$result['error'] = 'The request did not contain all required parameters: '.$response;
		} elseif ($this->status == '401') {
			$result['error'] = 'API key was not provided or is invalid.';
		} elseif ($this->status == '420') {
			$result['error'] = 'Login failed.';
		} elseif ($this->status == '500') {
			$result['error'] = 'There was an unexpected error on our server. This should be very rare and if you see it please contact developer@rapleaf.com.';
		} elseif ($this->status == '520') {
			$result['error'] = 'There was an error while reading the contacts from the address book.';
		}
		return $result;
	}

	# Parse the xml response text into an associative array
	function xmlToObj($str) {
		$xml = simplexml_load_string($str);
		$result = array();
		foreach ($xml->contact as $contact) {
			$result[] = array('name' => (string) $contact['name'], 'email' => (string) $contact['email']);
		}
		return $result;
	}
	
	# Returns the xml response on success, sets the error message on failure
	function sendPostRequest($url, $post_data) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array("Authorization: ".$this->api_key, "Content-Type: application/x-www-form-urlencoded")
		);
		
		$data = curl_exec($ch);
		$this->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return $data;
	}
}
?>